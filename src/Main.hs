{-# Language FlexibleContexts #-}
module Main where

import Execute
import ProgramState
import Types.ProgramState
import Types.Direction
import Types.Location

import System.Environment
import System.Exit
import Control.Lens
import Control.Monad
import Control.Monad.Writer
import Control.Monad.State
import Data.Char

version :: String
version = "Revisit interpreter 0.1"

flags :: [(Char, String, String)]
flags =
  [ ('A', "ascii-out", "Outputs as ascii character codes.")
  , ('h', "help", "Prints this menu.")
  , ('v', "version", "Prints the version number of this interpreter.") 
  ]

helpMenu :: String
helpMenu = unlines $
  [ ""
  , "Revisit Programming Language"
  , "Usage"
  , "revisit [options] source_file args..."
  , ""
  ] ++
  [ "\t-" ++ shortFlag : ", --" ++ longFlag ++ "\t\t" ++ description
  | (shortFlag, longFlag, description) <- flags
  ]

isFlag :: String -> Bool
isFlag ('-' : x : xs) = notElem x "0123456789"
isFlag x = False

processFlag ::
  ( MonadWriter [ String ] m
  )
    => String -> m [ Char ]
processFlag ('-' : '-' : xs) =
  case
    [ shortName | (shortName, longName, _) <- flags, longName == xs ]
  of
    [] -> do
      tell [ "Unknown flag: --" ++ xs ]
      return []
    [a] ->
      return [a]
    _ -> do
      tell [ "Ambiguous flag: --" ++ xs ]
      return []
processFlag [ '-', x ]
  | elem x $ map (view _1) flags = return [x]
  | otherwise                    = do
    tell [ "Unknown flag: -" ++ [x] ]
    return []
processFlag ('-' : xs) = fmap concat $ sequence $ map (processFlag . ('-' :) . pure) xs
processFlag _ = error "Flag parsing error"


processInput ::
  ( MonadWriter [ String ] m
  )
    => String -> m [Integer]
processInput ""   = tell [ "Invalid input: "   ] >> return []
processInput "\"" = tell [ "Invalid input: \"" ] >> return []
processInput "'"  = tell [ "Invalid input: '"  ] >> return []
processInput (('"') : xs @ (_ : _))
  | last xs == '"' = return $ map (toInteger . ord) $ init xs
  | otherwise      = do
    tell [ "Invalid input: " ++ ('"' : xs) ]
    return []
processInput (('\'') : xs @ (_ : _))
  | last xs == '\'' = return $ map (toInteger . ord) $ init xs
  | otherwise       = do
    tell [ "Invalid input: " ++ ('\'' : xs) ]
    return []
processInput (x : xs)
  | elem x "0123456789-"
  , all isDigit xs       = return [ read (x : xs) ]
  | otherwise            = do
    tell [ "Invalid input: " ++ (x : xs) ]
    return []

writerToErrors :: Writer [String] s -> IO s
writerToErrors wm =
  case
    runWriter wm
  of
    (result, []) ->
      return result
    (_, errors) -> do
      mapM_ putStrLn errors
      exitFailure

writeOut :: [Char] -> [Integer] -> IO ()
writeOut flags stack
  | elem 'A' flags = putStr $ map (chr . fromInteger) $ reverse stack
  | otherwise      = print $ reverse stack

collectProcess ::
  ( Monad m
  )
    => (a -> m [b]) -> [a]  -> m [b]
collectProcess = (fmap concat .) . (sequence .) . map

runStartState flags startState = writeOut flags $ view leftStack $ execState execute startState

main :: IO ()
main = do
  cla <- getArgs
  flags <- writerToErrors $ collectProcess processFlag $ filter isFlag cla
  case
    flags
    of
      flags
        | elem 'h' flags -> putStrLn helpMenu
        | elem 'v' flags -> putStrLn version
        | otherwise ->
          case filter (not . isFlag) cla of
            [] -> putStrLn "No source file provided."
            (sourceFile : args) -> do
              input <- writerToErrors $ collectProcess processInput args
              source <- readFile sourceFile
              runStartState flags $
                makeStartState (X 0, Y 0) East source input
