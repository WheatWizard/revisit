module Direction where

import Types.Direction
import Types.Location

import Control.Applicative
import Control.Arrow
import Control.Lens
import Data.Array

wrap :: Array (X Int, Y Int) a -> (X Int, Y Int) -> (X Int, Y Int)
wrap program = uncurry (***) $ ((flip (liftA2 mod) . (+1)) *** (flip (liftA2 mod) . (+1))) (view _2 $ bounds program)

moveReadHead :: Array (X Int, Y Int) a -> ((X Int, Y Int), Direction) -> ((X Int, Y Int), Direction)
moveReadHead program (loc, dir) =
  flip (,) dir $
    wrap program $
      ($ loc) $
        case dir of
          North -> ( id         *** subtract 1 )
          South -> ( id         *** (+ 1)      )
          East  -> ( (+ 1)      *** id         )
          West  -> ( subtract 1 *** id         )

mirror1 :: Direction -> Direction
mirror1 East  = North
mirror1 North = East
mirror1 West  = South
mirror1 South = West

mirror2 :: Direction -> Direction
mirror2 East  = West
mirror2 North = North
mirror2 West  = East
mirror2 South = South
