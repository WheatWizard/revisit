module Location
  ( transpose
  ) where

import Types.Location

transpose :: (X a, Y b) -> (X b, Y a)
transpose (X a, Y b) = (X b, Y a)
