{-# Language DeriveFunctor #-}
{-# Language GeneralizedNewtypeDeriving #-}

module Types.Location
  ( X (..)
  , Y (..)
  ) where

{- Location is a type wrapper for locations.
 - Since these types can only be converted from one to the other delibrately ..
 - it becomes much harder to get accidental mix ups.
 -}

import Data.Ix
import Control.Applicative
import Control.Monad.Random

newtype X a = X a
  deriving
    ( Ix
    , Eq
    , Ord
    , Show
    , Functor
    , Random
    , Num
    )

newtype Y a = Y a
  deriving
    ( Ix
    , Eq
    , Ord
    , Show
    , Functor
    , Random
    , Num
    )

instance Applicative X where
  pure = X
  liftA2 f (X a) (X b) = pure $ f a b

instance Applicative Y where
  pure = Y
  liftA2 f (Y a) (Y b) = pure $ f a b
