module Types.Direction
  ( Direction (..)
  ) where

data Direction
  = North
  | South
  | East
  | West
  deriving
    ( Eq
    , Ord
    , Show
    )
