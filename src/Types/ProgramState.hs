{-# Language TemplateHaskell #-}

module Types.ProgramState
  ( ProgramState ( .. )
  , program
  , location
  , direction
  , leftStack
  , rightStack
  , stringMode
  ) where

import Data.Array
import Control.Lens

import Types.Direction
import Types.Location

data ProgramState = ProgramState
  { _program    :: Array (X Int, Y Int) (Char, Bool)
  , _location   :: (X Int, Y Int)
  , _direction  :: Direction
  , _leftStack  :: [Integer]
  , _rightStack :: [Integer]
  , _stringMode :: Bool
  }
  deriving
    ( Show
    )
makeLenses ''ProgramState
