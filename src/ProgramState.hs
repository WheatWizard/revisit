module ProgramState
  ( wrapBounds
  , viewSize
  , oper
  , onness
  , makeStartState
  , createProgramArray
  )
  where

import Types.Direction
import Types.ProgramState
import Types.Location

import Control.Applicative
import Control.Lens
import Control.Monad.State.Class
import Control.Monad.Random
import Data.Array
  ( Array
  , Ix
  , array
  , bounds
  , range
  , (!)
  , (//)
  )

-- | A lens for indexing arrays
arrayAt ::
  ( Ix i
  , Functor f
  )
    => i -> (a -> f a) -> (Array i a) -> f (Array i a)
arrayAt index f array =
  ((array //) . (: []) . (,) index) <$> f (array ! index)
  

generate ::
  ( Ix i
  )
    => (i, i) -> (i -> a) -> Array i a
generate bnds f = array bnds $ ((,) <*> f) <$> range bnds

safeIndex :: Int -> [a] -> Maybe a
safeIndex i l
  | i < length l = Just $ l !! i
  | otherwise    = Nothing

createProgramArray :: String -> Array (X Int, Y Int) (Char, Bool)
createProgramArray source =
  generate ((X 0, Y 0), (width - 1, height - 1)) genFunction
  where
    sourceLines = lines source
    height = Y $ length sourceLines
    width  = X $ maximum $ map length sourceLines
    genFunction (X x, Y y) =
      case
        safeIndex y sourceLines >>= safeIndex x 
      of
        Just a  -> (a,   False)
        Nothing -> (' ', False)

makeStartState :: (X Int, Y Int) -> Direction -> String -> [Integer] -> ProgramState
makeStartState loc dir source input =
  ProgramState
    { _program    = createProgramArray source
    , _location   = loc
    , _direction  = dir
    , _leftStack  = input
    , _rightStack = []
    , _stringMode = False
    }

wrapBounds :: ProgramState -> ProgramState
wrapBounds pState =
  over location (wrap $ viewSize pState) pState
  where
    wrap ::
      ( Integral a
      , Integral b
      )
        => (X a, Y b) -> (X a, Y b) -> (X a, Y b)
    wrap (x, y) (a, b) = (liftA2 mod a (x + 1), liftA2 mod b (y + 1))

oper ::
  ( Functor f
  )
    => (Char -> f Char) -> ProgramState -> f ProgramState
oper f pState =
  (`setOper` pState) <$> f (viewOper pState)

onness ::
  ( Functor f
  )
    => (Bool -> f Bool) -> ProgramState -> f ProgramState
onness f pState =
  (`setOn` pState) <$> f (viewOn pState)

viewOper :: ProgramState -> Char
viewOper pState =
  view (program . arrayAt (view location pState) . _1) pState

setOper :: Char -> ProgramState -> ProgramState
setOper c pState =
  set (program . arrayAt (view location pState) . _1) c pState

viewOn :: ProgramState -> Bool
viewOn pState =
  view (program . arrayAt (view location pState) . _2) pState

setOn :: Bool -> ProgramState -> ProgramState
setOn b pState =
  set (program . arrayAt (view location pState) . _2) b pState

viewSize :: ProgramState -> (X Int, Y Int)
viewSize = snd . bounds . view program
