{-# Language FlexibleContexts #-}
{-# Language Rank2Types #-}
module Execute where

import Direction
import Types.Direction
import Types.ProgramState
import ProgramState
import Location

import Control.Arrow
import Control.Monad
import Control.Monad.State.Class
import Control.Lens
import Data.Char

execute ::
  ( MonadState ProgramState m
  )
    => m ()
execute = do
  op     <- gets $ view oper
  status <- gets $ view onness
  case op of
    -- If we are on the terminate instruction ..
    '@' ->
      if
        status
      then
        -- terminate
        return ()
      else
        -- continue
        takeStep >> execute
    _ ->
      takeStep >> execute

takeStep ::
  ( MonadState ProgramState m
  )
    => m ()
takeStep = do
  status <- gets $ view onness
  if status
    then do
      sMode  <- gets $ view stringMode
      isQuot <- gets $ ('"' ==) . view oper
      if sMode && not isQuot
        then gets (toInteger . ord . view oper) >>= push leftStack
        else performOperation
    else return ()
  -- Toggle the current cell
  modify $ over onness not
  -- Move
  move


pop ::
  ( MonadState state m
  , Num a
  )
    => Lens state state [a] [a] -> m a
pop stack = do
  st <- gets $ view stack
  case st of
    [] ->
      return 0
    (a : b) -> do
      modify $ set stack b
      return a

push ::
  ( MonadState state m
  )
    => ASetter state state [a] [a] -> a -> m ()
push stack n = modify $ over stack (n :)

performOperation ::
  ( MonadState ProgramState m
  )
    => m ()
performOperation = do
  op <- gets $ view oper

  case op of
    -- Mirrors
    '\\' ->
      modify $ over direction (mirror2 . mirror1)
    '/' ->
      modify $ over direction mirror1
    '|' ->
      modify $ over direction mirror2
    -- Hard Redirects
    '^' ->
      modify $ set direction North
    '>' ->
      modify $ set direction East
    'v' ->
      modify $ set direction South
    '<' ->
      modify $ set direction West
    {-
    -- Doors
    '[' -> do
      dir <- gets $ view direction
      case dir of
        East -> modify $ set oper ']' . set direction West
        West -> modify $ set oper ']'
        _ -> return ()
    ']' -> do
      dir <- gets $ view direction
      case dir of
        East -> modify $ set oper '['
        West -> modify $ set oper '[' . set direction East
        _ -> return ()
     -}

    -- Jumps
    '!' ->
      move
    '?' -> do
      top <- pop leftStack
      case top of
        0 -> return ()
        _ -> move
    -- Stack manipulation
    '$' -> do
      s1 <- pop leftStack
      s2 <- pop leftStack
      push leftStack s2
      push leftStack s1
    ':' -> do
      lStack <- gets $ view leftStack
      case lStack of
        [] ->
          push leftStack 0
        (a : b) ->
          push leftStack a
    '(' ->
      pop leftStack >>= push rightStack
    ')' ->
      pop rightStack >>= push leftStack
    -- Operations
    '+' ->
      liftM2 (+) (pop leftStack) (pop leftStack) >>= push leftStack
    '*' ->
      liftM2 (*) (pop leftStack) (pop leftStack) >>= push leftStack
    '-' ->
      fmap negate (pop leftStack) >>= push leftStack
    -- Literals
    '0' ->
      push leftStack 0
    '1' ->
      push leftStack 1
    '2' ->
      push leftStack 2
    '3' ->
      push leftStack 3
    '4' ->
      push leftStack 4
    '5' ->
      push leftStack 5
    '6' ->
      push leftStack 6
    '7' ->
      push leftStack 7
    '8' ->
      push leftStack 8
    '9' ->
      push leftStack 9
    '"' ->
      modify $ over stringMode not
    -- Noops
    _ ->
      return ()

move ::
  ( MonadState ProgramState m
  )
    => m ()
move = do
  dir <- gets $ view direction
  modify $ over location $ case dir of
    North -> id *** (subtract 1)
    East  -> (+1) *** id
    South -> id *** (+1)
    West  -> (subtract 1) *** id
  modify wrapBounds
